# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'homestead/core/version'

Gem::Specification.new do |spec|
  spec.name          = "homestead-core"
  spec.version       = Homestead::Core::VERSION
  spec.authors       = ["Chris Kentfield"]
  spec.email         = ["kentfieldc@orvis.com"]
  spec.description   = %q{Non-Rails pieces}
  spec.summary       = %q{Non-Rails pieces}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'paperclip', '~> 3.5.1'
  spec.add_dependency 'activerecord', '~> 4.0.0'

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
